# miniProject9-sm957



### Requirements

Streamlit App with a Hugging Face Model

- Create a website using Streamlit
- Connect to an open source LLM (Hugging Face)
- Deploy model via Streamlit or other service (accessible via browser)

### Steps

1. Install Streamlit using pip3
    
    `pip3 install streamlit`
2. Add dependencies to requirements.txt and install the packages

    `pip3 install -r requirements.txt`
3. Create your app content in the py file.
```
def main():

    st.title('Chatbot')
    st.sidebar.header('About')
    st.sidebar.text('This is a chatbot powered by GPT model.')
    st.sidebar.header('Instructions')
    st.sidebar.text('Enter your question in the text box and click on Generate button.')

    st.header('Ask me something!')
    text = st.text_area("How can I help you today?", height=200)

    if st.button("Generate"):

        if text:
            with st.spinner('Generating...'):
                content = model(text, max_length=200, num_return_sequences=1)[0]['generated_text']
            st.success("Answer:")
            st.write(content)
        else:
            st.warning("Please enter something.")
```

4. Run the streamlit app locally

    `streamlit run streamlit_app.py`
5. Deploy the app in  https://share.streamlit.io/, create your app using existing repo.
6. Set the repo, branch, python file.

### URL

Link to the app: https://ids721-week9-eprc5ss7igiw5sbncdkmes.streamlit.app/

### Screenshot

- Test your app locally

![](pic/pic1.png)

- Create a new app using existing repo in streamlit

![](pic/pic2.png)

- Set the repo, branch, python file

![](pic/pic3.png)

- Streamlit app page

![](pic/pic4.png)

